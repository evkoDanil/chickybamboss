<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 22.03.2020
  Time: 1:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>
    <link rel="stylesheet" href="css/addStyle.css">
</head>
<body>
<div class="message">
    <%

        String login = (String)session.getAttribute("login");
        String password = (String)session.getAttribute("password");
        String sendButtonType = "";
        String updateButtonType = "";
        if(login!=null && password!=null){
            sendButtonType = "hidden";
            updateButtonType = "submit";
        }
        else{
            sendButtonType = "submit";
            updateButtonType = "hidden";
        }

        String errors = (String)request.getAttribute("errors");
        Cookie[] cookies = request.getCookies();
        if(request.getAttribute("userName")!=null){
            out.print(request.getAttribute("userName"));
        }
        if(request.getAttribute("successLogin")!=null){
            out.print(request.getAttribute("successLogin"));
        }
        if(request.getAttribute("wrongAuthData")!=null){
            out.print(request.getAttribute("wrongAuthData"));
        }
        String name = "Name";
        String mail = "example@mail.ru";
        String dob = "2000-07-08";
        String biography = "Your awesome biography";
        if(cookies!=null) {
            for (Cookie c : cookies) {
                if (c.getName().equals("name")) {
                    name = c.getValue();
                } else if (c.getName().equals("e-mail")) {
                    mail = c.getValue();
                } else if (c.getName().equals("Date-of-birth")) {
                    dob = c.getValue();
                } else if (c.getName().equals("biography")) {
                    biography = c.getValue();
                }
            }
        }

        String nameError = "";
        String biographyError = "";
        String mailError = "";
        String dobError = "";
        String genderError = "";
        String quantityError="";
        String superPowerError = "";
        String contractError = "";
        if(errors!=null&&errors.contains("nameError")){
            nameError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Заполните имя.</div>";
        }
        if(errors!=null&&errors.contains("biographyError")){
            biographyError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Заполните биографию </div>";
        }
        if(errors!=null&&errors.contains("mailError")){
            mailError =  "<div class=\"error\" style=\"color:red; font-size:10px;\">Заполните e-mail </div>";
        }
        if(errors!=null&&errors.contains("dobError")){
            dobError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Заполните Дату рождения </div>";
        }
        if(errors!=null&&errors.contains("dobError")){
            quantityError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Заполните поле конечностей </div>";
        }
        if(errors!=null&&errors.contains("genderError")){
            genderError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Выберите пол </div>";
        }
        if(errors!=null&&errors.contains("superPowerError")){
            superPowerError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Выберите суперсилу </div>";
        }
        if(errors!=null&&errors.contains("contractError")){
            contractError = "<div class=\"error\" style=\"color:red; font-size:10px;\">Ознакомьтесь с контрактом </div>";
        }
    %>
</div>
<div class = "content">
    <div class ="main-form">
        <form method="POST" action="/add">
            <input type="hidden" name="form" value="mainForm"/>
            <div class = "text-field">
                <label >
                    Имя
                    <br>
                    <input name="name" value="<%out.print(name);%>" >
                    <%out.print(nameError);%>
                </label>

                <br>

                <label>
                    e-mail
                    <br>
                    <input type="email" name="e-mail"  value="<%out.print(mail);%>">
                    <%out.print(mailError);%>
                </label>
                <br>

                <label>
                    Дата Рождения
                    <br>
                    <input name="Date-of-birth" type="date" value="<%out.print(dob);%>">
                    <%out.print(dobError);%>
                </label>
                <br>

                Пол
                <br>
                <label><input type="radio" name="radio-button1" value="Male" checked="checked">Мужской</label>
                <label><input type="radio" name="radio-button1" value="Female"> Женский</label>

                <br>
                <%out.print(genderError);%>

                Количество конечностей
                <br>
                <label><input type="radio" name="radio-button2" value="1">1</label>
                <label><input type="radio" name="radio-button2" value="2">2</label><br>
                <label><input type="radio" name="radio-button2" value="3">3</label>
                <label><input type="radio" name="radio-button2" value="4" checked>4</label><br>
                <label><input type="radio" name="radio-button2" value="more">Больше</label>
                <br>
                <%out.print(quantityError);%>

                <label><!--бессмертие, прохождение сквозь стены, левитация -->
                    Суперсила
                    <br>
                    <select name="superpower" multiple="multiple">
                        <option value="immortality">Бессмертие</option>
                        <option value="passing-through-walls" selected>Прохождение сквозь стены</option>
                        <option value="levitation">Левитация</option>
                    </select>
                </label>
                <%out.print(superPowerError);%>
                <br>
                <label>
                    Биография
                    <br>
                    <textarea  name="biography" ><%out.print(biography);%></textarea>
                    <%out.print(biographyError);%>
                </label>
                <br>

                <label>
                    <input type="checkbox" name="contract" checked>С контрактом ознакомлен
                </label>
                <br>
                <%out.print(contractError);%>

                <input type="<%out.print(sendButtonType);%>" name="formButton" value="Send">
                <input type="<%out.print(updateButtonType);%>" name="formButton" value="Edit" >
                <input type="<%out.print(updateButtonType);%>" value="Выйти" onclick="<%session.invalidate();%>" >
            </div>
        </form>
    </div>

</div>
<div class="back"><button onclick="location.href='/'">Главная</button></div>
</body>
</html>
