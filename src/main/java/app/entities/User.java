package app.entities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Objects;

public class User {
    private String name;
    private String mail;
    private String dob;
    private String gender;
    private String quantity;
    private String superPower;
    private String biography;
    private String contract;

    public User(){
        name = mail = dob = gender = quantity = superPower = biography = contract = null;
    }
    public User(String name, String mail, String dob, String gender, String quantity, String superPower, String biography, String contract){
        this.name = name;
        this.mail = mail;
        this.dob = dob;
        this.gender = gender;
        this.quantity = quantity;
        this.superPower = superPower;
        this.biography = biography;
        this.contract = contract;
    }
    public User(String name, String mail, String dob, String gender, String quantity, String superPower, String biography){
        this.name = name;
        this.mail = mail;
        this.dob = dob;
        this.gender = gender;
        this.quantity = quantity;
        this.superPower = superPower;
        this.biography = biography;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getSuperPower() {
        return superPower;
    }

    public void setSuperPower(String superPower) {
        this.superPower = superPower;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(mail, user.mail) &&
                Objects.equals(dob, user.dob) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(quantity, user.quantity) &&
                Objects.equals(superPower, user.superPower) &&
                Objects.equals(biography, user.biography) &&
                Objects.equals(contract, user.contract);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, mail, dob, gender, quantity, superPower, biography, contract);
    }
    public String printFields(){
        return name+" "+mail+" "+dob+" "+gender+" "+quantity+" "+superPower+" "+biography;
    }
    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", dob='" + dob + '\'' +
                ", gender='" + gender + '\'' +
                ", quantity='" + quantity + '\'' +
                ", superPower='" + superPower + '\'' +
                ", biography='" + biography + '\'' +
                ", contract='" + contract + '\'' +
                '}';
    }

}
