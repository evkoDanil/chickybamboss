package app.servlets;

import app.DBConnectionData;
import app.entities.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

public class AddServlet extends HttpServlet implements DBConnectionData {


    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/add.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("formButton");
        System.out.println(action);
        HttpSession session = req.getSession();
        String sessionLogin = session.getId();
        String sessionID = (String)req.getAttribute("sessionID");
        System.out.println(sessionID+" session Id");
        System.out.println(sessionLogin);
        Cookie nameCookie;
        Cookie mailCoolie;
        Cookie dobCookie;
        Cookie genderCookie;
        Cookie quantityCookie;
        Cookie superPowerCookie;
        Cookie biographyCookie;
        Cookie contractCookie;

        String login;
        String password;

        nameCookie = new Cookie("name", req.getParameter("name"));
        mailCoolie = new Cookie("e-mail", req.getParameter("e-mail"));
        dobCookie = new Cookie("Date-of-birth", req.getParameter("Date-of-birth"));
        genderCookie = new Cookie("gender", req.getParameter("radio-button1"));
        quantityCookie = new Cookie("quantity", req.getParameter("radio-button2"));
        superPowerCookie = new Cookie("superpower", req.getParameter("superpower"));
        biographyCookie = new Cookie("biography", req.getParameter("biography"));
        contractCookie = new Cookie("contract", req.getParameter("contract"));

        Cookie[] cookies = {nameCookie, mailCoolie, dobCookie, genderCookie, quantityCookie,
                superPowerCookie, biographyCookie, contractCookie};

        if(!sessionID.contains(sessionLogin) || sessionID == null) {
            if (validate(cookies)) {
                System.out.println("validation returned true");
                for (Cookie c : cookies) {
                    c.setMaxAge(60 * 60 * 24 * 365);
                    resp.addCookie(c);
                }

                System.out.println("session field \"Login\" was null");
                password = generatePassword();
                login = generateLogin();
                addUser(cookies, password, login);

                String user = "<div class=\"success\"> User " + nameCookie.getValue() + " has been successfully added in database with " +
                        "Login: <span style=\"color:red;\">" + login + "</span> and Password:" +
                        "       <span style=\"color:red;\">" + password + "</span> </div>";
                req.setAttribute("userName", user);
                doGet(req, resp);
            } else {

                String errors = catchErrors(cookies);
                req.setAttribute("userName", nameCookie.getValue());
                req.setAttribute("errors", errors);
                for (Cookie c : cookies) {
                    c.setMaxAge(-1);
                    resp.addCookie(c);
                }
                doGet(req, resp);
            }
        }
        else{
            if(mailCoolie.getValue().contains("@")) {
                updateUser(cookies);
            }
            doGet(req,resp);
        }


    }

    private void updateUser(Cookie[] cookies) {

        String name,mail, dob, gender, quantity, superPower, biography, contract;

        name = cookies[0].getValue();
        mail = cookies[1].getValue();
        dob = cookies[2].getValue();
        gender = cookies[3].getValue();
        quantity = cookies[4].getValue();
        superPower = cookies[5].getValue();
        biography = cookies[6].getValue();
        contract = cookies[7].getValue();
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        try {

            Connection con = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
            PreparedStatement stmt = con.prepareStatement("UPDATE site_user SET mail = ?, dob = ?, gender = ?," +
                                                                "quantity = ?, superPower = ?, biogrphy = ?, contract = ? WHERE name = ?");

            stmt.setString(1, mail);
            stmt.setString(2, dob);
            stmt.setString(3, gender);
            stmt.setString(4, quantity);
            stmt.setString(5, superPower);
            stmt.setString(6, biography);
            stmt.setString(7, contract);
            stmt.setString(8, name);
            ResultSet set = stmt.executeQuery();

            con.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    private void addUser(Cookie[] cookies,String password,String login) {
        String name,mail, dob, gender, quantity, superPower, biography, contract;

        name = cookies[0].getValue();
        mail = cookies[1].getValue();
        dob = cookies[2].getValue();
        gender = cookies[3].getValue();
        quantity = cookies[4].getValue();
        superPower = cookies[5].getValue();
        biography = cookies[6].getValue();
        contract = cookies[7].getValue();
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        try {

            Connection con = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
            PreparedStatement stmt = con.prepareStatement("INSERT INTO site_user(name," +
                    "mail," +
                    "date_of_birth," +
                    "gender," +
                    "quantity," +
                    "super_power," +
                    "biography," +
                    "contract," +
                    "password," +
                    "login) VALUES (?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, name);
            stmt.setString(2, mail);
            stmt.setString(3, dob);
            stmt.setString(4, gender);
            stmt.setString(5, quantity);
            stmt.setString(6, superPower);
            stmt.setString(7, biography);
            stmt.setString(8, contract);
            stmt.setString(9,password);
            stmt.setString(10,login);
            ResultSet set = stmt.executeQuery();

            con.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    private void updateCookies(Cookie[] cookies,HttpServletRequest req,HttpServletResponse resp) throws ServletException,IOException{
        cookies[0].setValue(req.getParameter("name"));
        cookies[1].setValue(req.getParameter("e-mail"));
        cookies[2].setValue(req.getParameter("Date-of-birth"));
        cookies[3].setValue( req.getParameter("radio-button1"));
        cookies[4].setValue( req.getParameter("radio-button2"));
        cookies[5].setValue( req.getParameter("superpower"));
        cookies[6].setValue(req.getParameter("biography"));
        cookies[7].setValue(req.getParameter("contract"));
    }
    private String catchErrors(Cookie[] cookies){
        String name,mail, dob, gender, quantity, superPower, biography, contract;
        String errors = "";
        name = mail = dob = gender = quantity = superPower = biography = contract = "";
        for(Cookie c : cookies){
            if(c.getName().equals("name")){
                name = c.getValue();
            }
            else if(c.getName().equals("e-mail")){
                mail = c.getValue();
            }
            else if(c.getName().equals("Date-of-birth")){
                dob = c.getValue();
            }
            else if(c.getName().equals("biography")){
                biography = c.getValue();
            }
            else if(c.getName().equals("gender")){
                gender = c.getValue();
            }
            else if(c.getName().equals("contract")){
                contract = c.getValue();
            }
            else if(c.getName().equals("quantity")){
                quantity = c.getValue();
            }
            else if(c.getName().equals("superPower")){
                superPower = c.getValue();
            }
        }

        if(name==null || name.length()<3){
            errors+="nameError ";
        }
        if(!mail.contains("@") || !checkMail(mail) || mail == null || mail.length()<3){
            errors+="mailError ";
        }
        if(dob==null || dob.equals("")){
            errors+="dobError ";
        }
        if(gender==null|| gender.equals("")){
            errors+="genderError ";
        }
        if(quantity== null || quantity.equals("")){
            errors+="quantityError ";
        }
        if(superPower==null || superPower.equals("")){
            errors+="superPowerError ";
        }
        if(biography.length()<=10){
            errors+="biographyError " ;
        }
        if(contract==null || contract.equals("")){
            errors+="contractError";
        }
        return errors;
    }
    private boolean validate(Cookie[] cookies) {
        System.out.println("Validation in progress");
        String name,mail, dob, gender, quantity, superPower, biography, contract;

        name = cookies[0].getValue();
        mail = cookies[1].getValue();
        dob = cookies[2].getValue();
        gender = cookies[3].getValue();
        quantity = cookies[4].getValue();
        superPower = cookies[5].getValue();
        biography = cookies[6].getValue();
        contract = cookies[7].getValue();
        if(!name.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/") && !mail.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/") && !dob.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/")
            && !gender.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/") && !quantity.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/") &&
                !superPower.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/") && !biography.contains("/^[a-zA-Z0-9_-.]+@[a-z.]/")) {
            if (name != null && mail != null && dob != null && gender != null && quantity != null
                    && superPower != null && biography != null && contract != null && mail.contains("@") && biography.length() > 10 && checkMail(mail)) {
                return true;
            }
            else return false;
        }
        else return false;
    }
    private boolean checkMail(String mail){
        try{
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        try{
            Connection con = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
            PreparedStatement stmt = con.prepareStatement("SELECT mail FROM site_user WHERE mail = (?)");
            stmt.setString(1,mail);
            ResultSet res = stmt.executeQuery();
            con.close();
            while(res.next()){
                if(res.getString(1) != null)
                    return false;

            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return true;
    }
    private static int getRandomLength(int a,int b){
        return  a + (int) (Math.random() * b);
    }
    private String generateLogin(){
        int length = getRandomLength(1,15);
        String s = "";
        String firstLetter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String letters = "abcdefghijklmnopqrstuvwxyz";
        s+=firstLetter.charAt(getRandomLength(0,25));
        for(int i = 1;i<length;i++){
            s+=letters.charAt(getRandomLength(0,25));
        }
        return s;
    }
    private String generatePassword(){
        int length = getRandomLength(5,15);
        String s = "";
        String firstLetter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String letters = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        s+=firstLetter.charAt(getRandomLength(0,25));
        for(int i = 5;i<length-3;i++){
            s+=letters.charAt(getRandomLength(0,25));
        }
        for(int i = 0;i<3;i++){
            s+=numbers.charAt(getRandomLength(0,9));
        }
        return s;
    }
}

