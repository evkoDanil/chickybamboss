package app.servlets;

import app.DBConnectionData;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;

public class EnterServlet extends HttpServlet implements DBConnectionData {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/enter.jsp");
        requestDispatcher.forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        String action = req.getParameter("formButton");
        System.out.println(action);
        Cookie nameCookie;
        Cookie mailCoolie;
        Cookie dobCookie;
        Cookie genderCookie;
        Cookie quantityCookie;
        Cookie superPowerCookie;
        Cookie biographyCookie;
        Cookie contractCookie;

        String password = req.getParameter("password");
        String login = req.getParameter("userLogin");
        HttpSession session = req.getSession();
        if(isAdmin(login,password)) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/views/list.jsp");
            requestDispatcher.forward(req,resp);
        }
        else
            {
            if (isRightUser(login, password)) {
                session.setAttribute("login", login);
                session.setAttribute("password", password);
                System.out.println(session.getId());
                String successLogin = "<div> Вы успешно авторизовались, " + login + "</div>";
                req.setAttribute("successLogin", successLogin);
                String[] userData = getFields(login);
                nameCookie = new Cookie("name", userData[0]);
                mailCoolie = new Cookie("e-mail", userData[1]);
                dobCookie = new Cookie("Date-of-birth", userData[2]);
                genderCookie = new Cookie("gender", userData[3]);
                quantityCookie = new Cookie("quantity", userData[4]);
                superPowerCookie = new Cookie("superpower", userData[5]);
                biographyCookie = new Cookie("biography", userData[6]);


                nameCookie.setMaxAge(60 * 60 * 24 * 365);
                mailCoolie.setMaxAge(60 * 60 * 24 * 365);
                dobCookie.setMaxAge(60 * 60 * 24 * 365);
                genderCookie.setMaxAge(60 * 60 * 24 * 365);
                quantityCookie.setMaxAge(60 * 60 * 24 * 365);
                superPowerCookie.setMaxAge(60 * 60 * 24 * 365);
                biographyCookie.setMaxAge(60 * 60 * 24 * 365);
                resp.addCookie(nameCookie);
                resp.addCookie(mailCoolie);
                resp.addCookie(dobCookie);
                resp.addCookie(genderCookie);
                resp.addCookie(quantityCookie);
                resp.addCookie(superPowerCookie);
                resp.addCookie(biographyCookie);

                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/views/add.jsp");
                requestDispatcher.forward(req, resp);
            } else if (login == null && password == null) {
                String emptyAuthData = "<div> Вы забыли ввести данные </div>";
                req.setAttribute("wrongAuthData", emptyAuthData);
                doGet(req, resp);
            } else {
                String wrongAuthData = "<div> Вы ввели неверные данные </div>";
                req.setAttribute("wrongAuthData", wrongAuthData);
                doGet(req, resp);
            }

        }
    }
    private boolean isAdmin(String login,String password){
        int loginHash = login.hashCode();
        int passwordHash = password.hashCode();
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        try {

            Connection con = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            PreparedStatement stmt = con.prepareStatement("select loginHash,passwordHash from admins");
            ResultSet res = stmt.executeQuery();
            while(res.next()){
                if(login.contains(res.getString(1)) && password.contains(res.getString(2)))
                    return true;
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return false;
    }
    private String[] getFields(String login){
        String[] result = new String[7];
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        try {

            Connection con = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            PreparedStatement stmt = con.prepareStatement("select name, mail, date_of_birth, gender, quantity," +
                    "super_power, biography from site_user where login = ?");
            stmt.setString(1,login);
            ResultSet res = stmt.executeQuery();
            while(res.next()){
                result[0] = res.getString(1);
                result[1] = res.getString(2);
                result[2] = res.getString(3);
                result[3] = res.getString(4);
                result[4] = res.getString(5);
                result[5] = res.getString(6);
                result[6] = res.getString(7);
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return result;
    }
    private boolean isRightUser(String login,String password){
        String queryPassword = null;
        String queryLogin = null;
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        try {

            Connection con = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            PreparedStatement stmt = con.prepareStatement("select password, login from site_user " +
                    "where password = ? and login = ?");
            stmt.setString(1,password);
            stmt.setString(2,login);

            ResultSet res = stmt.executeQuery();

            while(res.next()){
                queryPassword = res.getString(1);
                queryLogin = res.getString(2);
            }

        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        if(queryLogin != null && queryPassword!= null){
            return queryLogin.equals(login) && queryPassword.equals(password);
        }
        else return false;
    }

}
