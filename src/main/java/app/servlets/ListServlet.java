package app.servlets;

import app.DBConnectionData;
import app.entities.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;

public class ListServlet extends HttpServlet implements DBConnectionData {
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

        createList(req);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/list.jsp");
        requestDispatcher.forward(req,resp);

    }
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        User u = (User)req.getAttribute("del");
        try{
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        try {
            Connection con = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            PreparedStatement stmt = con.prepareStatement("delete from user_list where id = ?");
            stmt.setString(1,u.getName());
            stmt.executeQuery();
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
    }
    private void createList(HttpServletRequest req){
        try{
            Class.forName(DRIVER);
        }
        catch(ClassNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        try{
            Connection con = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD) ;
            PreparedStatement stmt = con.prepareStatement("SELECT name,mail,date_of_birth,gender,quantity," +
                    "super_power,biography FROM site_user");
            ResultSet res = stmt.executeQuery();
            ArrayList<User> userList = new ArrayList<>();
            con.close();
            while(res.next()){
                String name = res.getString(1);
                String mail = res.getString(2);
                String dob = res.getString(3);
                String gender = res.getString(4);
                String quantity = res.getString(5);
                String superPower = res.getString(6);
                String biography = res.getString(7);
                User user = new User(name,mail,dob,gender,quantity,superPower,biography);
                userList.add(user);
                req.setAttribute("userList",userList);

            }

        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
    }
}
